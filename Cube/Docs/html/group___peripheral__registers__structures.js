var group___peripheral__registers__structures =
[
    [ "ADC_TypeDef", "struct_a_d_c___type_def.html", [
      [ "CR1", "struct_a_d_c___type_def.html#ab0ec7102960640751d44e92ddac994f0", null ],
      [ "CR2", "struct_a_d_c___type_def.html#afdfa307571967afb1d97943e982b6586", null ],
      [ "DR", "struct_a_d_c___type_def.html#a3df0d8dfcd1ec958659ffe21eb64fa94", null ],
      [ "HTR", "struct_a_d_c___type_def.html#a24c3512abcc90ef75cf3e9145e5dbe9b", null ],
      [ "JDR1", "struct_a_d_c___type_def.html#a22fa21352be442bd02f9c26a1013d598", null ],
      [ "JDR2", "struct_a_d_c___type_def.html#ae9156af81694b7a85923348be45a2167", null ],
      [ "JDR3", "struct_a_d_c___type_def.html#a3a54028253a75a470fccf841178cba46", null ],
      [ "JDR4", "struct_a_d_c___type_def.html#a9274ceea3b2c6d5c1903d0a7abad91a1", null ],
      [ "JOFR1", "struct_a_d_c___type_def.html#a427dda1678f254bd98b1f321d7194a3b", null ],
      [ "JOFR2", "struct_a_d_c___type_def.html#a11e65074b9f06b48c17cdfa5bea9f125", null ],
      [ "JOFR3", "struct_a_d_c___type_def.html#a613f6b76d20c1a513976b920ecd7f4f8", null ],
      [ "JOFR4", "struct_a_d_c___type_def.html#a2fd59854223e38158b4138ee8e913ab3", null ],
      [ "JSQR", "struct_a_d_c___type_def.html#a75e0cc079831adcc051df456737d3ae4", null ],
      [ "LTR", "struct_a_d_c___type_def.html#a9f8712dfef7125c0bb39db11f2b7416b", null ],
      [ "SMPR1", "struct_a_d_c___type_def.html#af9d6c604e365c7d9d7601bf4ef373498", null ],
      [ "SMPR2", "struct_a_d_c___type_def.html#a6ac83fae8377c7b7fcae50fa4211b0e8", null ],
      [ "SQR1", "struct_a_d_c___type_def.html#a3302e1bcfdfbbfeb58779d0761fb377c", null ],
      [ "SQR2", "struct_a_d_c___type_def.html#aab440b0ad8631f5666dd32768a89cf60", null ],
      [ "SQR3", "struct_a_d_c___type_def.html#a97e40d9928fa25a5628d6442f0aa6c0f", null ],
      [ "SR", "struct_a_d_c___type_def.html#af6aca2bbd40c0fb6df7c3aebe224a360", null ]
    ] ],
    [ "ADC_Common_TypeDef", "struct_a_d_c___common___type_def.html", [
      [ "CR1", "struct_a_d_c___common___type_def.html#ab0ec7102960640751d44e92ddac994f0", null ],
      [ "CR2", "struct_a_d_c___common___type_def.html#afdfa307571967afb1d97943e982b6586", null ],
      [ "DR", "struct_a_d_c___common___type_def.html#a3df0d8dfcd1ec958659ffe21eb64fa94", null ],
      [ "RESERVED", "struct_a_d_c___common___type_def.html#a5c0b11410c7c2684d58b63ee2ecdb398", null ],
      [ "SR", "struct_a_d_c___common___type_def.html#af6aca2bbd40c0fb6df7c3aebe224a360", null ]
    ] ],
    [ "BKP_TypeDef", "struct_b_k_p___type_def.html", [
      [ "CR", "struct_b_k_p___type_def.html#ab40c89c59391aaa9d9a8ec011dd0907a", null ],
      [ "CSR", "struct_b_k_p___type_def.html#a876dd0a8546697065f406b7543e27af2", null ],
      [ "DR1", "struct_b_k_p___type_def.html#a4bddb62706c830de59e263433390fcdb", null ],
      [ "DR10", "struct_b_k_p___type_def.html#ac9eaa42cf764059ca20a1daf7edba3e0", null ],
      [ "DR2", "struct_b_k_p___type_def.html#a9effac7d19f2ebb5ee12c3ce9f1803df", null ],
      [ "DR3", "struct_b_k_p___type_def.html#a68d30ac2fd332f01053ffce72825b171", null ],
      [ "DR4", "struct_b_k_p___type_def.html#ad4799a7d3d3678afbc29fcf623d510ed", null ],
      [ "DR5", "struct_b_k_p___type_def.html#a0d74106035c98f6d85139e38a929bcb5", null ],
      [ "DR6", "struct_b_k_p___type_def.html#a08d4980767b6b3aa6e49ac0b0ef65eb3", null ],
      [ "DR7", "struct_b_k_p___type_def.html#a0fa6ac40e28a7f29faa43f2bda29ef0a", null ],
      [ "DR8", "struct_b_k_p___type_def.html#a03f8be5d7c11b47ee08d2fcf2949c233", null ],
      [ "DR9", "struct_b_k_p___type_def.html#a7fa71253d3e565d39c9d5a309998cb27", null ],
      [ "RESERVED0", "struct_b_k_p___type_def.html#af86c61a5d38a4fc9cef942a12744486b", null ],
      [ "RTCCR", "struct_b_k_p___type_def.html#a6b0901e580e8884d2b4c525e43df90c7", null ]
    ] ],
    [ "CAN_TxMailBox_TypeDef", "struct_c_a_n___tx_mail_box___type_def.html", [
      [ "TDHR", "struct_c_a_n___tx_mail_box___type_def.html#a90f7c1cf22683459c632d6040366eddf", null ],
      [ "TDLR", "struct_c_a_n___tx_mail_box___type_def.html#aded1359e1a32512910bff534d57ade68", null ],
      [ "TDTR", "struct_c_a_n___tx_mail_box___type_def.html#aed87bed042dd9523ce086119a3bab0ea", null ],
      [ "TIR", "struct_c_a_n___tx_mail_box___type_def.html#a6921aa1c578a7d17c6e0eb33a73b6630", null ]
    ] ],
    [ "CAN_FIFOMailBox_TypeDef", "struct_c_a_n___f_i_f_o_mail_box___type_def.html", [
      [ "RDHR", "struct_c_a_n___f_i_f_o_mail_box___type_def.html#a7f11f42ba9d3bc5cd4a4f5ea0214608e", null ],
      [ "RDLR", "struct_c_a_n___f_i_f_o_mail_box___type_def.html#ae1c569688eedd49219cd505b9c22121b", null ],
      [ "RDTR", "struct_c_a_n___f_i_f_o_mail_box___type_def.html#a9563d8a88d0db403b8357331bea83a2e", null ],
      [ "RIR", "struct_c_a_n___f_i_f_o_mail_box___type_def.html#a0acc8eb90b17bef5b9e03c7ddaacfb0b", null ]
    ] ],
    [ "CAN_FilterRegister_TypeDef", "struct_c_a_n___filter_register___type_def.html", [
      [ "FR1", "struct_c_a_n___filter_register___type_def.html#a92036953ac673803fe001d843fea508b", null ],
      [ "FR2", "struct_c_a_n___filter_register___type_def.html#a7f7d80b45b7574463d7030fc8a464582", null ]
    ] ],
    [ "CAN_TypeDef", "struct_c_a_n___type_def.html", [
      [ "BTR", "struct_c_a_n___type_def.html#a5c0fcd3e7b4c59ab1dd68f6bd8f74e07", null ],
      [ "ESR", "struct_c_a_n___type_def.html#a2b39f943954e0e7d177b511d9074a0b7", null ],
      [ "FA1R", "struct_c_a_n___type_def.html#aaf76271f4ab0b3deb3ceb6e2ac0d62d0", null ],
      [ "FFA1R", "struct_c_a_n___type_def.html#af1405e594e39e5b34f9499f680157a25", null ],
      [ "FM1R", "struct_c_a_n___type_def.html#aaa6f4cf1f16aaa6d17ec6c410db76acf", null ],
      [ "FMR", "struct_c_a_n___type_def.html#a1cb734df34f6520a7204c4c70634ebba", null ],
      [ "FS1R", "struct_c_a_n___type_def.html#aae0256ae42106ee7f87fc7e5bdb779d4", null ],
      [ "IER", "struct_c_a_n___type_def.html#a6566f8cfbd1d8aa7e8db046aa35e77db", null ],
      [ "MCR", "struct_c_a_n___type_def.html#a27af4e9f888f0b7b1e8da7e002d98798", null ],
      [ "MSR", "struct_c_a_n___type_def.html#acdd4c1b5466be103fb2bb2a225b1d3a9", null ],
      [ "RESERVED0", "struct_c_a_n___type_def.html#aae28ab86a4ae57ed057ed1ea89a6d34b", null ],
      [ "RESERVED1", "struct_c_a_n___type_def.html#a4bb07a7828fbd5fe86f6a5a3545c177d", null ],
      [ "RESERVED2", "struct_c_a_n___type_def.html#a4c9b972a304c0e08ca27cbe57627c496", null ],
      [ "RESERVED3", "struct_c_a_n___type_def.html#af2b40c5e36a5e861490988275499e158", null ],
      [ "RESERVED4", "struct_c_a_n___type_def.html#ac0018930ee9f18afda25b695b9a4ec16", null ],
      [ "RESERVED5", "struct_c_a_n___type_def.html#a269f31b91d0f38a48061b76ecc346f55", null ],
      [ "RF0R", "struct_c_a_n___type_def.html#accf4141cee239380d0ad4634ee21dbf6", null ],
      [ "RF1R", "struct_c_a_n___type_def.html#a02b589bb589df4f39e549dca4d5abb08", null ],
      [ "sFIFOMailBox", "struct_c_a_n___type_def.html#a21b030b34e131f7ef6ea273416449fe4", null ],
      [ "sFilterRegister", "struct_c_a_n___type_def.html#a6f34c431b88ced05a7315dfd33b343b2", null ],
      [ "sTxMailBox", "struct_c_a_n___type_def.html#ae37503ab1a7bbd29846f94cdadf0a9ef", null ],
      [ "TSR", "struct_c_a_n___type_def.html#a87e3001757a0cd493785f1f3337dd0e8", null ]
    ] ],
    [ "CRC_TypeDef", "struct_c_r_c___type_def.html", [
      [ "CR", "struct_c_r_c___type_def.html#ab40c89c59391aaa9d9a8ec011dd0907a", null ],
      [ "DR", "struct_c_r_c___type_def.html#a3df0d8dfcd1ec958659ffe21eb64fa94", null ],
      [ "IDR", "struct_c_r_c___type_def.html#a601d7b0ba761c987db359b2d7173b7e0", null ],
      [ "RESERVED0", "struct_c_r_c___type_def.html#aa7d2bd5481ee985778c410a7e5826b71", null ],
      [ "RESERVED1", "struct_c_r_c___type_def.html#a8249a3955aace28d92109b391311eb30", null ]
    ] ],
    [ "DBGMCU_TypeDef", "struct_d_b_g_m_c_u___type_def.html", [
      [ "CR", "struct_d_b_g_m_c_u___type_def.html#ab40c89c59391aaa9d9a8ec011dd0907a", null ],
      [ "IDCODE", "struct_d_b_g_m_c_u___type_def.html#a24df28d0e440321b21f6f07b3bb93dea", null ]
    ] ],
    [ "DMA_Channel_TypeDef", "struct_d_m_a___channel___type_def.html", [
      [ "CCR", "struct_d_m_a___channel___type_def.html#a5e1322e27c40bf91d172f9673f205c97", null ],
      [ "CMAR", "struct_d_m_a___channel___type_def.html#ab51edd49cb9294ebe2db18fb5cf399dd", null ],
      [ "CNDTR", "struct_d_m_a___channel___type_def.html#aae019365e4288337da20775971c1a123", null ],
      [ "CPAR", "struct_d_m_a___channel___type_def.html#a07aacf332c9bf310ff5be02140f892e1", null ]
    ] ],
    [ "DMA_TypeDef", "struct_d_m_a___type_def.html", [
      [ "IFCR", "struct_d_m_a___type_def.html#ac6f9d540fd6a21c0fbc7bfbbee9a8504", null ],
      [ "ISR", "struct_d_m_a___type_def.html#ab3c49a96815fcbee63d95e1e74f20e75", null ]
    ] ],
    [ "EXTI_TypeDef", "struct_e_x_t_i___type_def.html", [
      [ "EMR", "struct_e_x_t_i___type_def.html#a6034c7458d8e6030f6dacecf0f1a3a89", null ],
      [ "FTSR", "struct_e_x_t_i___type_def.html#aa0f7c828c46ae6f6bc9f66f11720bbe6", null ],
      [ "IMR", "struct_e_x_t_i___type_def.html#ae845b86e973b4bf8a33c447c261633f6", null ],
      [ "PR", "struct_e_x_t_i___type_def.html#af8d25514079514d38c104402f46470af", null ],
      [ "RTSR", "struct_e_x_t_i___type_def.html#a0d952a17455687d6e9053730d028fa1d", null ],
      [ "SWIER", "struct_e_x_t_i___type_def.html#a9eae93b6cc13d4d25e12f2224e2369c9", null ]
    ] ],
    [ "FLASH_TypeDef", "struct_f_l_a_s_h___type_def.html", [
      [ "ACR", "struct_f_l_a_s_h___type_def.html#a9cb55206b29a8c16354747c556ab8bea", null ],
      [ "AR", "struct_f_l_a_s_h___type_def.html#a2ac50357d1ebac2949d27bfc4855e6a4", null ],
      [ "CR", "struct_f_l_a_s_h___type_def.html#ab40c89c59391aaa9d9a8ec011dd0907a", null ],
      [ "KEYR", "struct_f_l_a_s_h___type_def.html#a84c491be6c66b1d5b6a2efd0740b3d0c", null ],
      [ "OBR", "struct_f_l_a_s_h___type_def.html#aadec05237ee04c5a913c7ca9cd944f38", null ],
      [ "OPTKEYR", "struct_f_l_a_s_h___type_def.html#afc4900646681dfe1ca43133d376c4423", null ],
      [ "RESERVED", "struct_f_l_a_s_h___type_def.html#ad21eb922f00d583e80943def27a0f05c", null ],
      [ "SR", "struct_f_l_a_s_h___type_def.html#af6aca2bbd40c0fb6df7c3aebe224a360", null ],
      [ "WRPR", "struct_f_l_a_s_h___type_def.html#a9bc0e514c0860e3c153a6cfa72bdf1c3", null ]
    ] ],
    [ "OB_TypeDef", "struct_o_b___type_def.html", [
      [ "Data0", "struct_o_b___type_def.html#a76b31b1239a451ee8f397289265bdf6b", null ],
      [ "Data1", "struct_o_b___type_def.html#aa081efb0cb15b6ffd486d23a89144142", null ],
      [ "RDP", "struct_o_b___type_def.html#ae708f301b866ad2a81ed39efba639aab", null ],
      [ "USER", "struct_o_b___type_def.html#a67442d4e459bba2c40fa62914d78ec1e", null ],
      [ "WRP0", "struct_o_b___type_def.html#ad43c7a196f0eef88b3038383c4f7d903", null ],
      [ "WRP1", "struct_o_b___type_def.html#ac4e091dcb644dbb5d4a2c7aca7d4fe88", null ],
      [ "WRP2", "struct_o_b___type_def.html#a05486d021c6761bf5a04f410a1c24e06", null ],
      [ "WRP3", "struct_o_b___type_def.html#a7d9c1634a4c6027e12345f25d9d15b4d", null ]
    ] ],
    [ "GPIO_TypeDef", "struct_g_p_i_o___type_def.html", [
      [ "BRR", "struct_g_p_i_o___type_def.html#a092e59d908b2ca112e31047e942340cb", null ],
      [ "BSRR", "struct_g_p_i_o___type_def.html#ac25dd6b9e3d55e17589195b461c5ec80", null ],
      [ "CRH", "struct_g_p_i_o___type_def.html#acc2f82d559cfd955b5a68c1b54c5fc35", null ],
      [ "CRL", "struct_g_p_i_o___type_def.html#ae3e1b95965ce8c9f06490047cb9967a9", null ],
      [ "IDR", "struct_g_p_i_o___type_def.html#a328d2fe9ef1d513c3a97d30f98f0047c", null ],
      [ "LCKR", "struct_g_p_i_o___type_def.html#a2612a0f4b3fbdbb6293f6dc70105e190", null ],
      [ "ODR", "struct_g_p_i_o___type_def.html#abff7fffd2b5a718715a130006590c75c", null ]
    ] ],
    [ "AFIO_TypeDef", "struct_a_f_i_o___type_def.html", [
      [ "EVCR", "struct_a_f_i_o___type_def.html#ade0e8d03661dcb48d09182f4beb41b32", null ],
      [ "EXTICR", "struct_a_f_i_o___type_def.html#a52f7bf8003ba69d66a4e86dea6eeab65", null ],
      [ "MAPR", "struct_a_f_i_o___type_def.html#a9f3b56f295c863e23ed4c82380b1d621", null ],
      [ "MAPR2", "struct_a_f_i_o___type_def.html#a12f2941917fe1e4000af10d1793f8e49", null ],
      [ "RESERVED0", "struct_a_f_i_o___type_def.html#af86c61a5d38a4fc9cef942a12744486b", null ]
    ] ],
    [ "I2C_TypeDef", "struct_i2_c___type_def.html", [
      [ "CCR", "struct_i2_c___type_def.html#a5e1322e27c40bf91d172f9673f205c97", null ],
      [ "CR1", "struct_i2_c___type_def.html#ab0ec7102960640751d44e92ddac994f0", null ],
      [ "CR2", "struct_i2_c___type_def.html#afdfa307571967afb1d97943e982b6586", null ],
      [ "DR", "struct_i2_c___type_def.html#a3df0d8dfcd1ec958659ffe21eb64fa94", null ],
      [ "OAR1", "struct_i2_c___type_def.html#a08b4be0d626a00f26bc295b379b3bba6", null ],
      [ "OAR2", "struct_i2_c___type_def.html#ab5c57ffed0351fa064038939a6c0bbf6", null ],
      [ "SR1", "struct_i2_c___type_def.html#acefca4fd83c4b7846ae6d3cfe7bb8df9", null ],
      [ "SR2", "struct_i2_c___type_def.html#a89623ee198737b29dc0a803310605a83", null ],
      [ "TRISE", "struct_i2_c___type_def.html#a5d5764c0ec44b661da957e6343f9e7b5", null ]
    ] ],
    [ "IWDG_TypeDef", "struct_i_w_d_g___type_def.html", [
      [ "KR", "struct_i_w_d_g___type_def.html#a2f692354bde770f2a5e3e1b294ec064b", null ],
      [ "PR", "struct_i_w_d_g___type_def.html#af8d25514079514d38c104402f46470af", null ],
      [ "RLR", "struct_i_w_d_g___type_def.html#a7015e1046dbd3ea8783b33dc11a69e52", null ],
      [ "SR", "struct_i_w_d_g___type_def.html#af6aca2bbd40c0fb6df7c3aebe224a360", null ]
    ] ],
    [ "PWR_TypeDef", "struct_p_w_r___type_def.html", [
      [ "CR", "struct_p_w_r___type_def.html#ab40c89c59391aaa9d9a8ec011dd0907a", null ],
      [ "CSR", "struct_p_w_r___type_def.html#a876dd0a8546697065f406b7543e27af2", null ]
    ] ],
    [ "RCC_TypeDef", "struct_r_c_c___type_def.html", [
      [ "AHBENR", "struct_r_c_c___type_def.html#acdf2b32fb3d8dad6bee74bf4cbe25020", null ],
      [ "APB1ENR", "struct_r_c_c___type_def.html#ac88901e2eb35079b7b58a185e6bf554c", null ],
      [ "APB1RSTR", "struct_r_c_c___type_def.html#a7da5d372374bc59e9b9af750b01d6a78", null ],
      [ "APB2ENR", "struct_r_c_c___type_def.html#acc7bb47dddd2d94de124f74886d919be", null ],
      [ "APB2RSTR", "struct_r_c_c___type_def.html#ab2c5389c9ff4ac188cd498b8f7170968", null ],
      [ "BDCR", "struct_r_c_c___type_def.html#a0b9a3ced775287c8585a6a61af4b40e9", null ],
      [ "CFGR", "struct_r_c_c___type_def.html#a26f1e746ccbf9c9f67e7c60e61085ec1", null ],
      [ "CIR", "struct_r_c_c___type_def.html#a907d8154c80b7e385478943f90b17a3b", null ],
      [ "CR", "struct_r_c_c___type_def.html#ab40c89c59391aaa9d9a8ec011dd0907a", null ],
      [ "CSR", "struct_r_c_c___type_def.html#a876dd0a8546697065f406b7543e27af2", null ]
    ] ],
    [ "RTC_TypeDef", "struct_r_t_c___type_def.html", [
      [ "ALRH", "struct_r_t_c___type_def.html#a2525206b87d6b19a6d0b565c99f711f8", null ],
      [ "ALRL", "struct_r_t_c___type_def.html#a13af31dc4addc634ff6d562cd00ac1e6", null ],
      [ "CNTH", "struct_r_t_c___type_def.html#abeeeb09de7999ec993e00d136f679de0", null ],
      [ "CNTL", "struct_r_t_c___type_def.html#aaea1cf878c3e7ab37309e673eb50101b", null ],
      [ "CRH", "struct_r_t_c___type_def.html#acc2f82d559cfd955b5a68c1b54c5fc35", null ],
      [ "CRL", "struct_r_t_c___type_def.html#ae3e1b95965ce8c9f06490047cb9967a9", null ],
      [ "DIVH", "struct_r_t_c___type_def.html#a0c7aed7845db21cbed704a636f53e023", null ],
      [ "DIVL", "struct_r_t_c___type_def.html#a36bb5c9678921fde2b5bc811d310a8c5", null ],
      [ "PRLH", "struct_r_t_c___type_def.html#a6d8529957b9401e614203b2389f290a4", null ],
      [ "PRLL", "struct_r_t_c___type_def.html#a4d5d3c1969bb190e095335b98d11c197", null ]
    ] ],
    [ "SPI_TypeDef", "struct_s_p_i___type_def.html", [
      [ "CR1", "struct_s_p_i___type_def.html#ab0ec7102960640751d44e92ddac994f0", null ],
      [ "CR2", "struct_s_p_i___type_def.html#afdfa307571967afb1d97943e982b6586", null ],
      [ "CRCPR", "struct_s_p_i___type_def.html#ace450027b4b33f921dd8edd3425a717c", null ],
      [ "DR", "struct_s_p_i___type_def.html#a3df0d8dfcd1ec958659ffe21eb64fa94", null ],
      [ "I2SCFGR", "struct_s_p_i___type_def.html#aa0c41c8883cb0812d6aaf956c393584b", null ],
      [ "RXCRCR", "struct_s_p_i___type_def.html#a2cf9dcd9008924334f20f0dc6b57042e", null ],
      [ "SR", "struct_s_p_i___type_def.html#af6aca2bbd40c0fb6df7c3aebe224a360", null ],
      [ "TXCRCR", "struct_s_p_i___type_def.html#ab4e4328504fd66285df8264d410deefd", null ]
    ] ],
    [ "TIM_TypeDef", "struct_t_i_m___type_def.html", [
      [ "ARR", "struct_t_i_m___type_def.html#af17f19bb4aeea3cc14fa73dfa7772cb8", null ],
      [ "BDTR", "struct_t_i_m___type_def.html#a476bae602205d6a49c7e71e2bda28c0a", null ],
      [ "CCER", "struct_t_i_m___type_def.html#a098110becfef10e1fd1b6a4f874da496", null ],
      [ "CCMR1", "struct_t_i_m___type_def.html#adb72f64492a75e780dd2294075c70fed", null ],
      [ "CCMR2", "struct_t_i_m___type_def.html#a091452256c9a16c33d891f4d32b395bf", null ],
      [ "CCR1", "struct_t_i_m___type_def.html#adab1e24ef769bbcb3e3769feae192ffb", null ],
      [ "CCR2", "struct_t_i_m___type_def.html#ab90aa584f07eeeac364a67f5e05faa93", null ],
      [ "CCR3", "struct_t_i_m___type_def.html#a27a478cc47a3dff478555ccb985b06a2", null ],
      [ "CCR4", "struct_t_i_m___type_def.html#a85fdb75569bd7ea26fa48544786535be", null ],
      [ "CNT", "struct_t_i_m___type_def.html#a6095a27d764d06750fc0d642e08f8b2a", null ],
      [ "CR1", "struct_t_i_m___type_def.html#ab0ec7102960640751d44e92ddac994f0", null ],
      [ "CR2", "struct_t_i_m___type_def.html#afdfa307571967afb1d97943e982b6586", null ],
      [ "DCR", "struct_t_i_m___type_def.html#af6225cb8f4938f98204d11afaffd41c9", null ],
      [ "DIER", "struct_t_i_m___type_def.html#a07fccbd85b91e6dca03ce333c1457fcb", null ],
      [ "DMAR", "struct_t_i_m___type_def.html#ab9087f2f31dd5edf59de6a59ae4e67ae", null ],
      [ "EGR", "struct_t_i_m___type_def.html#a196ebdaac12b21e90320c6175da78ef6", null ],
      [ "OR", "struct_t_i_m___type_def.html#a75ade4a9b3d40781fd80ce3e6589e98b", null ],
      [ "PSC", "struct_t_i_m___type_def.html#a9d4c753f09cbffdbe5c55008f0e8b180", null ],
      [ "RCR", "struct_t_i_m___type_def.html#aa1b1b7107fcf35abe39d20f5dfc230ee", null ],
      [ "SMCR", "struct_t_i_m___type_def.html#a2870732a4fc2ecd7bbecfbcbbf5528b7", null ],
      [ "SR", "struct_t_i_m___type_def.html#af6aca2bbd40c0fb6df7c3aebe224a360", null ]
    ] ],
    [ "USART_TypeDef", "struct_u_s_a_r_t___type_def.html", [
      [ "BRR", "struct_u_s_a_r_t___type_def.html#a092e59d908b2ca112e31047e942340cb", null ],
      [ "CR1", "struct_u_s_a_r_t___type_def.html#ab0ec7102960640751d44e92ddac994f0", null ],
      [ "CR2", "struct_u_s_a_r_t___type_def.html#afdfa307571967afb1d97943e982b6586", null ],
      [ "CR3", "struct_u_s_a_r_t___type_def.html#add5b8e29a64c55dcd65ca4201118e9d1", null ],
      [ "DR", "struct_u_s_a_r_t___type_def.html#a3df0d8dfcd1ec958659ffe21eb64fa94", null ],
      [ "GTPR", "struct_u_s_a_r_t___type_def.html#a5dd0cb6c861eaf26470f56f451c1edbf", null ],
      [ "SR", "struct_u_s_a_r_t___type_def.html#af6aca2bbd40c0fb6df7c3aebe224a360", null ]
    ] ],
    [ "USB_TypeDef", "struct_u_s_b___type_def.html", [
      [ "BTABLE", "struct_u_s_b___type_def.html#a045130eb8c2d3eb353a4417f24f34f46", null ],
      [ "CNTR", "struct_u_s_b___type_def.html#a532529aa4c809b7e6881ce081f55c37b", null ],
      [ "DADDR", "struct_u_s_b___type_def.html#ae95ebb359f2974e0f25b27e488978922", null ],
      [ "EP0R", "struct_u_s_b___type_def.html#a325f0bdb1f81ce237dea2773bc26aed2", null ],
      [ "EP1R", "struct_u_s_b___type_def.html#a181159566b312dd1471e247e6a74b8ef", null ],
      [ "EP2R", "struct_u_s_b___type_def.html#aaf056ff97c76de78e90701449c8cbf16", null ],
      [ "EP3R", "struct_u_s_b___type_def.html#ac4d0c88deada778ef870d2f6d478768f", null ],
      [ "EP4R", "struct_u_s_b___type_def.html#a304267e30a8fb671cfe22c8ef965d284", null ],
      [ "EP5R", "struct_u_s_b___type_def.html#a5c7950efccc55900c811a434d259e357", null ],
      [ "EP6R", "struct_u_s_b___type_def.html#aba6ced7617c465949dc6b9ba64b96ef7", null ],
      [ "EP7R", "struct_u_s_b___type_def.html#abc8d8ef89e886cc3492e0617bef98edf", null ],
      [ "FNR", "struct_u_s_b___type_def.html#a5ecb1ade997ff76fd2ff76370717d464", null ],
      [ "ISTR", "struct_u_s_b___type_def.html#a54fc7329a9549448d56b50bcca73bab4", null ],
      [ "RESERVED0", "struct_u_s_b___type_def.html#ab234cb4952ccf50c24a841b3f2f28a91", null ],
      [ "RESERVED1", "struct_u_s_b___type_def.html#abd0cb7c1fef737616a25adb37ef909bd", null ],
      [ "RESERVED2", "struct_u_s_b___type_def.html#a44086b7a050f78b2148a60945e477293", null ],
      [ "RESERVED3", "struct_u_s_b___type_def.html#a73aa1eb05d9f4581a6efe7a8e919bcbf", null ],
      [ "RESERVED4", "struct_u_s_b___type_def.html#a27f713a0ca762e5fd478a66a82f39a36", null ],
      [ "RESERVED5", "struct_u_s_b___type_def.html#a71145eb8e6d24d871c231d38f2ff49f7", null ],
      [ "RESERVED6", "struct_u_s_b___type_def.html#af6264c920c71ea17140e0f673bd2f9ca", null ],
      [ "RESERVED7", "struct_u_s_b___type_def.html#a56ede38a529e8da85e8ac7497a342486", null ],
      [ "RESERVED8", "struct_u_s_b___type_def.html#adcfbdb4bcad575e5ed423803e0a0b321", null ],
      [ "RESERVED9", "struct_u_s_b___type_def.html#ac72f7489a9d2c795bce1158ee23e78f3", null ],
      [ "RESERVEDA", "struct_u_s_b___type_def.html#aaa3240660b9b6f379ecdffd4d440f726", null ],
      [ "RESERVEDB", "struct_u_s_b___type_def.html#a952c408bacd5ee662017440986eca043", null ],
      [ "RESERVEDC", "struct_u_s_b___type_def.html#a9af23dac80d50cdf937b9fbbc4add948", null ]
    ] ],
    [ "WWDG_TypeDef", "struct_w_w_d_g___type_def.html", [
      [ "CFR", "struct_w_w_d_g___type_def.html#ac011ddcfe531f8e16787ea851c1f3667", null ],
      [ "CR", "struct_w_w_d_g___type_def.html#ab40c89c59391aaa9d9a8ec011dd0907a", null ],
      [ "SR", "struct_w_w_d_g___type_def.html#af6aca2bbd40c0fb6df7c3aebe224a360", null ]
    ] ]
];