var indexSectionsWithContent =
{
  0: "_abcdefghiklmnopqrstuvwxz",
  1: "_abcdefgilnoprstuwx",
  2: "cmst",
  3: "_abdehilmnopstu",
  4: "_abcdefghiklmnopqrstuvwxz",
  5: "dt",
  6: "efghist",
  7: "abcdefhimnprstuw",
  8: "_ahlmptv",
  9: "abcdefghilmnoprstu",
  10: "dlm"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "files",
  3: "functions",
  4: "variables",
  5: "typedefs",
  6: "enums",
  7: "enumvalues",
  8: "defines",
  9: "groups",
  10: "pages"
};

var indexSectionLabels =
{
  0: "All",
  1: "Data Structures",
  2: "Files",
  3: "Functions",
  4: "Variables",
  5: "Typedefs",
  6: "Enumerations",
  7: "Enumerator",
  8: "Macros",
  9: "Modules",
  10: "Pages"
};

