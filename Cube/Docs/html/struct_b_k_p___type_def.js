var struct_b_k_p___type_def =
[
    [ "CR", "struct_b_k_p___type_def.html#ab40c89c59391aaa9d9a8ec011dd0907a", null ],
    [ "CSR", "struct_b_k_p___type_def.html#a876dd0a8546697065f406b7543e27af2", null ],
    [ "DR1", "struct_b_k_p___type_def.html#a4bddb62706c830de59e263433390fcdb", null ],
    [ "DR10", "struct_b_k_p___type_def.html#ac9eaa42cf764059ca20a1daf7edba3e0", null ],
    [ "DR2", "struct_b_k_p___type_def.html#a9effac7d19f2ebb5ee12c3ce9f1803df", null ],
    [ "DR3", "struct_b_k_p___type_def.html#a68d30ac2fd332f01053ffce72825b171", null ],
    [ "DR4", "struct_b_k_p___type_def.html#ad4799a7d3d3678afbc29fcf623d510ed", null ],
    [ "DR5", "struct_b_k_p___type_def.html#a0d74106035c98f6d85139e38a929bcb5", null ],
    [ "DR6", "struct_b_k_p___type_def.html#a08d4980767b6b3aa6e49ac0b0ef65eb3", null ],
    [ "DR7", "struct_b_k_p___type_def.html#a0fa6ac40e28a7f29faa43f2bda29ef0a", null ],
    [ "DR8", "struct_b_k_p___type_def.html#a03f8be5d7c11b47ee08d2fcf2949c233", null ],
    [ "DR9", "struct_b_k_p___type_def.html#a7fa71253d3e565d39c9d5a309998cb27", null ],
    [ "RESERVED0", "struct_b_k_p___type_def.html#af86c61a5d38a4fc9cef942a12744486b", null ],
    [ "RTCCR", "struct_b_k_p___type_def.html#a6b0901e580e8884d2b4c525e43df90c7", null ]
];