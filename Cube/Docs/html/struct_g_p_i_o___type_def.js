var struct_g_p_i_o___type_def =
[
    [ "BRR", "struct_g_p_i_o___type_def.html#a092e59d908b2ca112e31047e942340cb", null ],
    [ "BSRR", "struct_g_p_i_o___type_def.html#ac25dd6b9e3d55e17589195b461c5ec80", null ],
    [ "CRH", "struct_g_p_i_o___type_def.html#acc2f82d559cfd955b5a68c1b54c5fc35", null ],
    [ "CRL", "struct_g_p_i_o___type_def.html#ae3e1b95965ce8c9f06490047cb9967a9", null ],
    [ "IDR", "struct_g_p_i_o___type_def.html#a328d2fe9ef1d513c3a97d30f98f0047c", null ],
    [ "LCKR", "struct_g_p_i_o___type_def.html#a2612a0f4b3fbdbb6293f6dc70105e190", null ],
    [ "ODR", "struct_g_p_i_o___type_def.html#abff7fffd2b5a718715a130006590c75c", null ]
];