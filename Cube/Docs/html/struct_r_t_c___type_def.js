var struct_r_t_c___type_def =
[
    [ "ALRH", "struct_r_t_c___type_def.html#a2525206b87d6b19a6d0b565c99f711f8", null ],
    [ "ALRL", "struct_r_t_c___type_def.html#a13af31dc4addc634ff6d562cd00ac1e6", null ],
    [ "CNTH", "struct_r_t_c___type_def.html#abeeeb09de7999ec993e00d136f679de0", null ],
    [ "CNTL", "struct_r_t_c___type_def.html#aaea1cf878c3e7ab37309e673eb50101b", null ],
    [ "CRH", "struct_r_t_c___type_def.html#acc2f82d559cfd955b5a68c1b54c5fc35", null ],
    [ "CRL", "struct_r_t_c___type_def.html#ae3e1b95965ce8c9f06490047cb9967a9", null ],
    [ "DIVH", "struct_r_t_c___type_def.html#a0c7aed7845db21cbed704a636f53e023", null ],
    [ "DIVL", "struct_r_t_c___type_def.html#a36bb5c9678921fde2b5bc811d310a8c5", null ],
    [ "PRLH", "struct_r_t_c___type_def.html#a6d8529957b9401e614203b2389f290a4", null ],
    [ "PRLL", "struct_r_t_c___type_def.html#a4d5d3c1969bb190e095335b98d11c197", null ]
];