var struct_____d_m_a___handle_type_def =
[
    [ "ChannelIndex", "struct_____d_m_a___handle_type_def.html#ae2d85e64eb57a8bccd3f70e74db09c31", null ],
    [ "DmaBaseAddress", "struct_____d_m_a___handle_type_def.html#adaadecc05539a447843606e1e511d992", null ],
    [ "ErrorCode", "struct_____d_m_a___handle_type_def.html#a123c5063e6a3b1901b2fbe5f88c53a7e", null ],
    [ "Init", "struct_____d_m_a___handle_type_def.html#a4352c7144ad5e1e4ab54a87d3be6eb62", null ],
    [ "Instance", "struct_____d_m_a___handle_type_def.html#adc5940b5bbf1fc712118cd40ff3ea69b", null ],
    [ "Lock", "struct_____d_m_a___handle_type_def.html#ad4cf225029dbefe8d3fe660c33b8bb6b", null ],
    [ "Parent", "struct_____d_m_a___handle_type_def.html#a6ee5f2130887847bbc051932ea43b73d", null ],
    [ "State", "struct_____d_m_a___handle_type_def.html#ad41e111a28e868b2df5bbce9925cf42d", null ],
    [ "XferAbortCallback", "struct_____d_m_a___handle_type_def.html#ac8b9c6b8bd238d961f18d14feb0b8ef3", null ],
    [ "XferCpltCallback", "struct_____d_m_a___handle_type_def.html#a509ad1cf1f44fc48289c3984747c096c", null ],
    [ "XferErrorCallback", "struct_____d_m_a___handle_type_def.html#ae7e9d51c7e7f450dc91d688229765c84", null ],
    [ "XferHalfCpltCallback", "struct_____d_m_a___handle_type_def.html#a2c9d8dd548db8d8d26db94c90bdc6805", null ]
];