var main_8h =
[
    [ "LED_GPIO_Port", "main_8h.html#a3cf393d21586a4097159246e8aca13a2", null ],
    [ "LED_Pin", "main_8h.html#ac969c4fccd009dd70b69992d92cbe81a", null ],
    [ "StatusTimerUpdate", "main_8h.html#aeed717fec636ca320c2c6302f313c3f8", [
      [ "UPDATE_OK", "main_8h.html#aeed717fec636ca320c2c6302f313c3f8aba01c4f0b477f5ef6796d095a32779d4", null ],
      [ "INVALID_PARAMS", "main_8h.html#aeed717fec636ca320c2c6302f313c3f8acf84273eb1a0660491b87c5a62b1f128", null ],
      [ "UPDATE_ERROR", "main_8h.html#aeed717fec636ca320c2c6302f313c3f8ada3e6c59b895f66f5b6678668bffe721", null ]
    ] ],
    [ "Tim2Period", "main_8h.html#af5113511a8352d1f543732bcc2b247ae", [
      [ "PERIOD_200_MS", "main_8h.html#af5113511a8352d1f543732bcc2b247aea0efcfc79d597a544d7f70bf19fb0cd19", null ],
      [ "PERIOD_100_MS", "main_8h.html#af5113511a8352d1f543732bcc2b247aeab72c60c96df38506ab7d978fdb34b167", null ]
    ] ],
    [ "Error_Handler", "main_8h.html#a1730ffe1e560465665eb47d9264826f9", null ],
    [ "updateTimerPeriod", "main_8h.html#ab299c504e0a138ce8a471274c7ddeba5", null ]
];