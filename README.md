# Grechushkin_traning_prjct

Git for D.Grechushkin training

## Name
Blinking LED with the ability to change the brightness at the touch of a button

## Description
Configure the project and study principles of working with PLL, GPIO and TIM (PWM) for STM32 MCU

1) Configure the necessary peripherals for your MCU with STM32CubeMX:
	- Configure the clocking system:
        	- Set the desired clocking frequency for SYSCLK and HCLK;
        	- Use external quartz resonator (if available);
	- Configuration of the debug interface (JTAG, SWD);
	- Configuration of the necessary GPIOs;
	- The configuration of the required TIM;
2) Using a hardware timer, implement the task of blinking the LED at intervals of 200ms on, 100ms off.
3) Using a hardware timer configured as a PWM source, implement the task of changing the brightness of the LED by pressing a button. At each press change the PWM fill factor from 0 to 100% in steps of 10%. When you reach 100% fill factor the next push of the button should set the fill factor to 0%, then 10%, then 20% and so on. For the key it is necessary to provide a software protection mechanism against contact bouncing (if it is not provided by the hardware).


Настройка проекта и изучение принципов работы с PLL, GPIO и TIM (PWM) для STM32 MCU

1) Сконфигурировать необходимую периферию для своего MCU с помощью STM32CubeMX:
	- Конфигурация системы тактирования:
        	- установить желаемую частоту тактирования для SYSCLK и HCLK;
        	- использовать внешний кварцевый резонатор (если есть возможность);
	- Конфигурация отладочного интерфейса (JTAG, SWD);
	- Конфигурация необходимых GPIO;
	- Конфигурация необходимого TIM;
2) С использованием аппаратного таймера реализовать задачу мигания светодиодом с интервалами 200ms горит, 100ms не горит.
3) С использованием аппаратного таймера, настроенного в качестве ШИМ (PWM) источника, реализовать задачу изменения яркости свечения светодиода по нажатию кнопки. При каждом нажатии изменять коэффициент заполнения PWM от 0 до 100% с шагом 10%. При достижении коэфф. заполнения 100% следующее нажатие клавиши должно установить коэфф. зап. 0%, затем 10%, затем 20% и так покругу. Для клавиши необходимо предусмотреть программный механизм защиты от дребезга контактов (если таковой не предусмотрен аппаратно).

## Authors and acknowledgment
Dmitriy Grchushkin

