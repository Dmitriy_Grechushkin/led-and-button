/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  *
  * Author: dmitriy
  * Created on: Oct 12, 2022
  *
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "init.h"

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/

StatusButton_t ButtonState;
PwmLed_t PmwLed;

volatile FlagInterrupt_t ItFlagButton = FLAG_DOWN;
volatile FlagInterrupt_t ItFlagBlink = FLAG_DOWN;
volatile FlagInterrupt_t ItFlagDebounce = FLAG_DOWN;

/* Private function prototypes -----------------------------------------------*/
/* Private user code ---------------------------------------------------------*/

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* MCU Configuration--------------------------------------------------------*/
  if (initializeMcu() != MCU_INIT_OK) {
	  Error_Handler();
  }

  if (initializePwmLed(&PmwLed, getTimPwmHandle(), getTimPwmChannel(), MAX_DUTY_CYCLE) != UPDATE_OK) {
	  Error_Handler();
  }
  ButtonState = initializeButtonState();


  /* update and launch timer led blinking */
  HAL_TIM_Base_Start_IT(getTimBlinkHandle());

  /* Infinite loop */
  while (1)
  {

	  if (IS_INTERRUPT_FLAG_UP(ItFlagButton)) {

		  /* Start the timer that checks of button debounce - 10 ms */
		  HAL_TIM_Base_Start_IT(getTimStateCheckHandle());

		  ItFlagButton = FLAG_DOWN;
	  }

	  if (IS_INTERRUPT_FLAG_UP(ItFlagBlink)) {

		  togglePwmLed(&PmwLed);
		  if (IS_LED_ON(PmwLed.State)) {

			  /* Update the timer for 200 ms */
			  if (updateTimerPeriod(PERIOD_200_MS) != UPDATE_OK) {
				  Error_Handler();
			  }
		  }
		  else if (IS_LED_OFF(PmwLed.State)) {

			  /* Update the timer for 100 ms */
			  if (updateTimerPeriod(PERIOD_100_MS) != UPDATE_OK) {
				  Error_Handler();
			  }
		  }

		  ItFlagBlink = FLAG_DOWN;
	  }

	  if (IS_INTERRUPT_FLAG_UP(ItFlagDebounce))
	  {
		  StatusStateCheck_t TempStatusChecking = checkButtonStateChange(ButtonState);

		  if (TempStatusChecking == CHANGED)
		  {
			  if (ButtonState == IDLE){
				  ButtonState = CLICK;

				  increasePwmLedPulse(&PmwLed);
				  if (updatePwmPulse(&PmwLed) != UPDATE_OK)
				  {
					  Error_Handler();
				  }
			  }
			  else if (ButtonState == CLICK)
			  {
				  ButtonState = IDLE;
			  }

			  /* Stop the timer waiting for the end of button debounce */
			  HAL_TIM_Base_Stop_IT(getTimStateCheckHandle());

			  /* Enable button interruptions */
			  HAL_NVIC_EnableIRQ(EXTI0_IRQn);
		  }
		  else if (TempStatusChecking == NO_CHANGED)
		  {

			  /* Stop the timer waiting for the end of button debounce */
			  HAL_TIM_Base_Stop_IT(getTimStateCheckHandle());

			  /* Enable button interruptions */
			  HAL_NVIC_EnableIRQ(EXTI0_IRQn);
		  }
		  ItFlagDebounce = FLAG_DOWN;
	  }
  }
}


/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */
  __disable_irq();
  while (1)
  {
  }
  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */
