/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.h
  * @brief          : Header for main.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  *
  * Author: dmitriy
  * Created on: Oct 12, 2022
  *
  ******************************************************************************
  */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32f4xx_hal.h"

/* Private includes ----------------------------------------------------------*/
/* Exported types ------------------------------------------------------------*/
/**
  * @brief  Result value update
  */
typedef enum
{
    UPDATE_OK = 0U,
    INVALID_PARAMS = 1U,
    UPDATE_ERROR = 2U
} StatusValueUpdate_t;

/**
  * @brief  Status Function Initialize
  */
typedef enum
{
    INIT_OK = 0U,
	INIT_ERROR = 1U,
} StatusFunctionInitialize_t;

/**
  * @brief  Status of interrupt flag
  */
typedef enum
{
    FLAG_DOWN = 0U,
    FLAG_UP = 1U
} FlagInterrupt_t;

/**
  * @brief State GPIO Led ON and Led OFF enumeration
  */
typedef enum
{
	LED_OFF = 0,
	LED_ON
} LedState_t;

/* Exported constants --------------------------------------------------------*/
/* Exported macro ------------------------------------------------------------*/

/****************** Interrupt Flag : check FLAG_UP state **********************/
#define IS_INTERRUPT_FLAG_UP(FLAG)    ((FLAG) == FLAG_UP)

/****************** Led State : check LED_ON state ****************************/
#define IS_LED_ON(LED_STATE)    ((LED_STATE) == LED_ON)

/****************** Led State : check LED_OFF state ****************************/
#define IS_LED_OFF(LED_STATE)    ((LED_STATE) == LED_OFF)

/* Exported functions prototypes ---------------------------------------------*/
void Error_Handler(void);

/* Private defines -----------------------------------------------------------*/
#define LED_Pin GPIO_PIN_6
#define LED_GPIO_Port GPIOA
#define BUTTON_Pin GPIO_PIN_0
#define BUTTON_GPIO_Port GPIOB
#define BUTTON_EXTI_IRQn EXTI0_IRQn
#define TMS_Pin GPIO_PIN_13
#define TMS_GPIO_Port GPIOA
#define TCK_Pin GPIO_PIN_14
#define TCK_GPIO_Port GPIOA

#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H */
