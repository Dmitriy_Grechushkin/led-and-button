/**
  ******************************************************************************
  * @file    init.c
  * @brief   Initialize Peripherals.
  * 		 Reset of all peripherals, Initializes the Flash interface and the
  * 		 Systick, Configure the system clock, Initialize all configured
  * 		 peripherals
  ******************************************************************************
  * @attention
  *
  * Author: dmitriy
  * Created on: Oct 12, 2022
  *
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include "init.h"

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
/* Private function prototypes -----------------------------------------------*/
/* Private user code ---------------------------------------------------------*/
/* External variables --------------------------------------------------------*/
/* External functions --------------------------------------------------------*/


/**
  * @brief  The application entry point.
  * @retval Init status
  */
InitErrors_t initializeMcu(void)
{
	/* Reset of all peripherals, Initializes the Flash interface and the Systick. */

	if (HAL_Init() != HAL_OK)
	{
	  return ERROR_HAL;
	}

	/* Configure the system clock */
	if (initializeSystemClock() != INIT_OK)
	{
		return ERROR_RCC;
	}

	/* Initialize all configured peripherals */
	if (initializeButton() != INIT_OK)
	{
		return ERROR_BUTTON;
	}
	if (initializeTimBlink() != INIT_OK)
	{
		return ERROR_TIM_BLINK;
	}
	if (initializeTimPwm() != INIT_OK)
	{
		return ERROR_TIM_PWM;
	}
	if (initializeTimDebounce() != INIT_OK)
	{
		return ERROR_TIM_DEBOUNCE;
	}

	return MCU_INIT_OK;
}

