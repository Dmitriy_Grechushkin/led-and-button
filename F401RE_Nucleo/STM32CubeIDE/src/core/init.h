/**
  ******************************************************************************
  * @file           : init.h
  * @brief          : Header for init.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  *
  * Author: dmitriy
  * Created on: Oct 12, 2022
  *
  ******************************************************************************
  */

#ifndef SRC_CORE_INIT_H_
#define SRC_CORE_INIT_H_


/* Includes ------------------------------------------------------------------*/
#include "stm32f4xx_hal.h"
#include "proj_drv_rcc.h"
#include "prj_drv_led_timer.h"
#include "prj_drv_led.h"
#include "prj_drv_button.h"

/* Exported types ------------------------------------------------------------*/

/**
  * @brief  Result initialization MCU
  */
typedef enum {
    MCU_INIT_OK = 0U,
    ERROR_HAL = 1U,
	ERROR_RCC = 1U,
	ERROR_BUTTON = 1U,
	ERROR_TIM_BLINK = 1U,
	ERROR_TIM_PWM = 1U,
	ERROR_TIM_DEBOUNCE = 2U
} InitErrors_t;

/* Exported constants --------------------------------------------------------*/
/* Exported macro ------------------------------------------------------------*/
/* Exported functions prototypes ---------------------------------------------*/
InitErrors_t initializeMcu(void);

/* Private defines -----------------------------------------------------------*/

#endif /* SRC_CORE_INIT_H_ */
