/**
  ******************************************************************************
  * @file           : proj_drv_rcc.h
  * @brief          : Header for proj_drv_rcc.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  *
  * Author: dmitriy
  * Created on: Oct 12, 2022
  *
  ******************************************************************************
  */
/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef PROJ_DRV_RCC_H_
#define PROJ_DRV_RCC_H_

/* Includes ------------------------------------------------------------------*/
#include "main.h"

/* Exported types ------------------------------------------------------------*/
/* Exported constants --------------------------------------------------------*/
/* Exported macro ------------------------------------------------------------*/
/* Exported functions prototypes ---------------------------------------------*/
StatusFunctionInitialize_t initializeSystemClock(void);

/* Private defines -----------------------------------------------------------*/


#endif /* PROJ_DRV_RCC_H_ */
