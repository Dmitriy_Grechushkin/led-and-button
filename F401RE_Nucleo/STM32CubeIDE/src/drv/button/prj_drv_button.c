/**
  ******************************************************************************
  * @file    prj_drv_button.c
  * @brief   Button Driver.
  ******************************************************************************
  * @attention
  *
  * Author: dmitriy
  * Created on: Oct 12, 2022
  *
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include "prj_drv_button.h"
#include "main.h"

/* Private define ------------------------------------------------------------*/
#define MAX_CHECK_NUMBER    8U
#define MAX_NUMBER_CONFIRMATIONS    5U

/* Private variables ---------------------------------------------------------*/

/* Counter of the number of button status checks */
static uint32_t CounterButtonStatusCheck = 0;

/* Counter of the success number of checks of the button state */
static uint32_t CounterSuccessCheck = 0;

/**
  * @brief GPIO Initialization Function
  * @param None
  * @retval None
  */
StatusFunctionInitialize_t initializeButton(void)
{
  GPIO_InitTypeDef GPIO_InitStruct = {0};

  /* GPIO Ports Clock Enable */
  __HAL_RCC_GPIOC_CLK_ENABLE();
  __HAL_RCC_GPIOH_CLK_ENABLE();
  __HAL_RCC_GPIOA_CLK_ENABLE();
  __HAL_RCC_GPIOB_CLK_ENABLE();

  /*Configure GPIO pin : BUTTON_Pin */
  GPIO_InitStruct.Pin = BUTTON_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_IT_RISING_FALLING;
  GPIO_InitStruct.Pull = GPIO_PULLDOWN;
  HAL_GPIO_Init(BUTTON_GPIO_Port, &GPIO_InitStruct);

  /* EXTI interrupt init*/
  HAL_NVIC_SetPriority(EXTI0_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(EXTI0_IRQn);

  return INIT_OK;

}
/**
  * @brief Initialization initial button state
  * @param None
  * @retval None
  */
StatusButton_t initializeButtonState(void)
{
	if (HAL_GPIO_ReadPin(BUTTON_GPIO_Port, BUTTON_Pin) == GPIO_PIN_SET)
	{
		return CLICK;
	}
	else
	{
		return IDLE;
	}
}

/**
  * @brief Check changing button state
  * @param None
  * @retval Status of checking
  */
StatusStateCheck_t checkButtonStateChange(StatusButton_t LastState)
{
	CounterButtonStatusCheck++;

	if (LastState == IDLE)
	{
		if (HAL_GPIO_ReadPin(BUTTON_GPIO_Port, BUTTON_Pin) == GPIO_PIN_SET)
		{
			CounterSuccessCheck++;
		}
	}
	else if (LastState == CLICK)
	{

		if (HAL_GPIO_ReadPin(BUTTON_GPIO_Port, BUTTON_Pin) == GPIO_PIN_RESET)
		{
			CounterSuccessCheck++;
		}

	}
	if (CounterSuccessCheck > MAX_NUMBER_CONFIRMATIONS)
	{
		CounterSuccessCheck = 0;
		CounterButtonStatusCheck = 0;

		return CHANGED;
	}
	if (CounterButtonStatusCheck > MAX_CHECK_NUMBER)
	{

		CounterSuccessCheck = 0;
		CounterButtonStatusCheck = 0;

		return NO_CHANGED;
	}

	return IN_PROCESS;
}



