/**
  ******************************************************************************
  * @file           : prj_drv_led_timer.h
  * @brief          : Header for prj_drv_led_timer.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  * @attention
  *
  * Author: dmitriy
  * Created on: Oct 12, 2022
  *
  ******************************************************************************
  */

#ifndef PRJ_DRV_LED_TIMER_H_
#define PRJ_DRV_LED_TIMER_H_

/* Includes ------------------------------------------------------------------*/
#include "main.h"

/* Exported types ------------------------------------------------------------*/
/* Exported constants --------------------------------------------------------*/
/* Exported macro ------------------------------------------------------------*/
/* Exported functions prototypes ---------------------------------------------*/
StatusValueUpdate_t updateTimerPeriod(uint32_t);

StatusFunctionInitialize_t initializeTimBlink(void);
StatusFunctionInitialize_t initializeTimDebounce(void);
TIM_HandleTypeDef* getTimBlinkHandle(void);
TIM_HandleTypeDef* getTimStateCheckHandle(void);

/* Private defines -----------------------------------------------------------*/

#endif /* PRJ_DRV_LED_TIMER_H_ */
