/**
  ******************************************************************************
  * @file    prj_drv_pwm.c
  * @brief   PWM Driver.
  ******************************************************************************
  *
  * Author: dmitriy
  * Created on: Oct 12, 2022
  *
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include "prj_drv_pwm.h"

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
static TIM_HandleTypeDef htim3;
static uint32_t Channel = TIM_CHANNEL_1;
static uint32_t FrequanceApb1 = 8000000;
static uint32_t CounterPeriod = 100;
static uint32_t FlickerFrequency = 250;

/* Private function prototypes -----------------------------------------------*/
/* Private user code ---------------------------------------------------------*/
/* External variables --------------------------------------------------------*/
/* External functions --------------------------------------------------------*/

/**
  * @brief TIM3 Initialization Function
  * @param None
  * @retval None
  */
StatusFunctionInitialize_t initializeTimPwm(void)
{
  TIM_ClockConfigTypeDef sClockSourceConfig = {0};
  TIM_MasterConfigTypeDef sMasterConfig = {0};
  TIM_OC_InitTypeDef sConfigOC = {0};

  htim3.Instance = TIM3;
  htim3.Init.Prescaler = (FrequanceApb1 / (FlickerFrequency * CounterPeriod)) - 1;
  htim3.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim3.Init.Period = CounterPeriod - 1;
  htim3.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  htim3.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
  if (HAL_TIM_Base_Init(&htim3) != HAL_OK)
  {
	  return INIT_ERROR;
  }
  sClockSourceConfig.ClockSource = TIM_CLOCKSOURCE_INTERNAL;
  if (HAL_TIM_ConfigClockSource(&htim3, &sClockSourceConfig) != HAL_OK)
  {
	  return INIT_ERROR;
  }
  if (HAL_TIM_PWM_Init(&htim3) != HAL_OK)
  {
	  return INIT_ERROR;
  }
  sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  if (HAL_TIMEx_MasterConfigSynchronization(&htim3, &sMasterConfig) != HAL_OK)
  {
	  return INIT_ERROR;
  }
  sConfigOC.OCMode = TIM_OCMODE_PWM1;
  sConfigOC.Pulse = DEFAULT_DUTY_CYCLE;
  sConfigOC.OCPolarity = TIM_OCPOLARITY_HIGH;
  sConfigOC.OCFastMode = TIM_OCFAST_DISABLE;
  if (HAL_TIM_PWM_ConfigChannel(&htim3, &sConfigOC, Channel) != HAL_OK)
  {
	  return INIT_ERROR;
  }
  HAL_TIM_MspPostInit(&htim3);

  if (HAL_TIM_PWM_Start(&htim3, Channel) != HAL_OK)
  {
	  return INIT_ERROR;
  }

  return INIT_OK;
}

/**
  * @brief  Get Handle of timer PWM
  * @retval Pointer to Handle of timer PWM
  */
TIM_HandleTypeDef* getTimPwmHandle(void)
{
	return &htim3;
}

/**
  * @brief  Get Handle of timer channel PWM
  * @retval Timer channel PWM
  */
uint32_t getTimPwmChannel(void)
{
	return Channel;
}

/**
  * @brief  Set pulse PWM
  * @param  Led: PwmLed object
  * @retval Status update PWM pulse
  */
StatusValueUpdate_t updatePwmPulse(PwmLed_t* Led)
{
	if ((Led->Pulse > MAX_DUTY_CYCLE) || (Led->PwmTimer == NULL))
	{
		return INVALID_PARAMS;
	}

	switch (Led->Channel)
	{
	    case TIM_CHANNEL_1:
	    {
	      /* Check the parameters */
	      assert_param(IS_TIM_CC1_INSTANCE(Led->PwmTimer->Instance));

	      /* Return the capture 1 value */
	      Led->PwmTimer->Instance->CCR1 = Led->Pulse;

	      break;
	    }
	    case TIM_CHANNEL_2:
	    {
	      /* Check the parameters */
	      assert_param(IS_TIM_CC2_INSTANCE(Led->PwmTimer->Instance));

	      /* Return the capture 2 value */
	      Led->PwmTimer->Instance->CCR2 = Led->Pulse;

	      break;
	    }

	    case TIM_CHANNEL_3:
	    {
	      /* Check the parameters */
	      assert_param(IS_TIM_CC3_INSTANCE(Led->PwmTimer->Instance));

	      /* Return the capture 3 value */
	      Led->PwmTimer->Instance->CCR3 = Led->Pulse;

	      break;
	    }

	    case TIM_CHANNEL_4:
	    {
	      /* Check the parameters */
	      assert_param(IS_TIM_CC4_INSTANCE(Led->PwmTimer->Instance));

	      /* Return the capture 4 value */
	      Led->PwmTimer->Instance->CCR4 = Led->Pulse;

	      break;
	    }

	    default:
	    {
	    	return INVALID_PARAMS;
	    	break;
	    }
	}
    return UPDATE_OK;
}

/**
  * @brief  Reset pulse PWM
  * @param  Led: PwmLed object
  * @retval Status update PWM pulse
  */
StatusValueUpdate_t resetPwmPulse(PwmLed_t* Led)
{
	if (Led->PwmTimer == NULL)
	{
		return INVALID_PARAMS;
	}

	HAL_TIM_PWM_Stop(Led->PwmTimer, Led->Channel);

    return UPDATE_OK;
}

/**
  * @brief  Restore pulse PWM
  * @param  Led: PwmLed object
  * @retval Status update PWM pulse
  */
StatusValueUpdate_t restorePwmPulse(PwmLed_t* Led)
{
	if (Led->PwmTimer == NULL)
	{
		return INVALID_PARAMS;
	}

	HAL_TIM_PWM_Start(Led->PwmTimer, Led->Channel);

    return UPDATE_OK;
}

/**
  * @brief Change Flicker Frequency PWM
  * @param  NewFlickerFrequency: New Flicker Frequency
  * @retval Status update Flicker Frequency
  */
StatusValueUpdate_t changeFlickerFrequency(uint32_t NewFlickerFrequency)
{
	if (HAL_TIM_PWM_Stop(&htim3, Channel) != HAL_OK)
	{
	  return UPDATE_ERROR;
	}

	FlickerFrequency = NewFlickerFrequency;
	initializeTimPwm();

    return UPDATE_OK;
}


