/**
  ******************************************************************************
  * @file           : prj_drv_pwm.h
  * @brief          : Header for prj_drv_pwm.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  * @attention
  *
  * Author: dmitriy
  * Created on: Oct 12, 2022
  *
  ******************************************************************************
  */
/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef PRJ_DRV_PWM_H_
#define PRJ_DRV_PWM_H_

/* Includes ------------------------------------------------------------------*/
#include "main.h"

/* Exported types ------------------------------------------------------------*/
/**
  * @brief PWM led initialize structure definition
  */
typedef struct
{

	LedState_t State;       /*!< Specifies the PWM led to be configured.
                           	   	    This parameter can be any value LED_ON or LED_OFF */

	TIM_HandleTypeDef* PwmTimer;      /*!< Specifies the TIM Time Base Handle Structure definition */

	uint32_t Channel;      /*!< Specifies the Pull-up or Pull-Down activation for the selected pins.
                           	    This parameter can be a value of @ref TIM_Channel */

	uint32_t Pulse;     /*!< Specifies the pulse for the PWM.
                           	 This parameter can be a value from 0 to MAX_PWM */

} PwmLed_t;

/* Exported constants --------------------------------------------------------*/
#define  MAX_DUTY_CYCLE         100U  /*!< max value of the PWM pulse */
#define  DEFAULT_DUTY_CYCLE         100U  /*!< default value of the PWM pulse */

/* Exported macro ------------------------------------------------------------*/


/* Exported functions prototypes ---------------------------------------------*/
TIM_HandleTypeDef* getTimPwmHandle(void);
uint32_t getTimPwmChannel(void);
StatusFunctionInitialize_t initializeTimPwm(void);
StatusValueUpdate_t updatePwmPulse(PwmLed_t*);
StatusValueUpdate_t resetPwmPulse(PwmLed_t*);
StatusValueUpdate_t restorePwmPulse(PwmLed_t*);
StatusValueUpdate_t changeFlickerFrequency(uint32_t);

/* Private defines -----------------------------------------------------------*/
/* External functions prototypes ---------------------------------------------*/
void HAL_TIM_MspPostInit(TIM_HandleTypeDef *htim);

#endif /* PRJ_DRV_PWM_H_ */
