/**
  ******************************************************************************
  * @file    prj_drv_led.c
  * @brief   Led Driver.
  ******************************************************************************
  * @attention
  *
  * Author: dmitriy
  * Created on: Oct 12, 2022
  *
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include "prj_drv_led.h"
#include "prj_drv_pwm.h"

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
#define  DUTY_CYCLE_SHIFT         10U    /*!< duty cycle shift */

/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
/* Private function prototypes -----------------------------------------------*/
/* Private user code ---------------------------------------------------------*/
/* External variables --------------------------------------------------------*/
/* External functions --------------------------------------------------------*/

/**
  * @brief Initialization Function of PWM Led
  * This function configures timer that change led state
  * @param Led: PwmLed object
  * @param htim: timer handle
  * @param Channel: channel of timer
  * @param Pulse: initial PWM pulse
  * @retval None
  */
StatusValueUpdate_t initializePwmLed(PwmLed_t* Led, TIM_HandleTypeDef* htim,
								   uint32_t Channel, uint16_t Pulse)
{
	if ((Led == NULL) || (htim == NULL) || (Pulse > MAX_DUTY_CYCLE) ||
		(!IS_TIM_CCX_INSTANCE(htim->Instance, Channel)))
	{
		return INVALID_PARAMS;
	}

	Led->State = LED_ON;
	Led->PwmTimer = htim;
	Led->Channel = Channel;
	Led->Pulse = Pulse;

	return UPDATE_OK;
}

/**
  * @brief Toggle PWM Led
  * @param Led: PwmLed object
  * @retval None
  */
StatusValueUpdate_t togglePwmLed(PwmLed_t* Led)
{
	if ((Led == NULL) || (Led->PwmTimer == NULL))
	{
		return INVALID_PARAMS;
	}

	if (Led->State == LED_ON) {
		Led->State = LED_OFF;

		/* Reset the PWM Pulse */
		if (resetPwmPulse(Led) != UPDATE_OK)
		{
			return UPDATE_ERROR;
		}
	}
	else if (Led->State == LED_OFF)
	{
		Led->State = LED_ON;

		/* Restore the PWM Pulse */
		if (restorePwmPulse(Led) != UPDATE_OK) {
			return UPDATE_ERROR;
		}
	}
	return UPDATE_OK;
}

/**
  * @brief Increase PWM Led Pulse
  * @param Led: PwmLed object
  * @retval Status of updating
  */
StatusValueUpdate_t increasePwmLedPulse(PwmLed_t* Led)
{
	if ((Led == NULL) || (Led->PwmTimer == NULL))
	{
		return INVALID_PARAMS;
	}

	Led->Pulse += DUTY_CYCLE_SHIFT;
	if (Led->Pulse > MAX_DUTY_CYCLE)
	{
		Led->Pulse = 0;
	}
	return UPDATE_OK;
}





