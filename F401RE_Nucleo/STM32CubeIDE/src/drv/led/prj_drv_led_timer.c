/**
  ******************************************************************************
  * @file    prj_drv_led_timer.c
  * @brief   Led Driver.
  ******************************************************************************
  * Author: dmitriy
  * Created on: Oct 12, 2022
  *
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include "prj_drv_led_timer.h"
#include "prj_drv_led.h"

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
static TIM_HandleTypeDef htim2;
static TIM_HandleTypeDef htim4;

static uint32_t BlinkPrescaller = 8 * 1000;
static uint32_t BlinkPeriod = 200;
static uint32_t DebouncePrescaller = 8 * 1000;
static uint32_t DebouncePeriod = 10;

/* Private function prototypes -----------------------------------------------*/
/* Private user code ---------------------------------------------------------*/
/* External variables --------------------------------------------------------*/
/* External functions --------------------------------------------------------*/


/**
  * @brief TIM2 Initialization Function
  * This function configures timer that change led state
  * @param None
  * @retval None
  */
StatusFunctionInitialize_t initializeTimBlink(void)
{
  TIM_ClockConfigTypeDef sClockSourceConfig = {0};
  TIM_MasterConfigTypeDef sMasterConfig = {0};

  htim2.Instance = TIM2;
  htim2.Init.Prescaler = BlinkPrescaller - 1;
  htim2.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim2.Init.Period = BlinkPeriod - 1;
  htim2.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  htim2.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
  if (HAL_TIM_Base_Init(&htim2) != HAL_OK)
  {
	  return INIT_ERROR;
  }
  sClockSourceConfig.ClockSource = TIM_CLOCKSOURCE_INTERNAL;
  if (HAL_TIM_ConfigClockSource(&htim2, &sClockSourceConfig) != HAL_OK)
  {
	  return INIT_ERROR;
  }
  sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  if (HAL_TIMEx_MasterConfigSynchronization(&htim2, &sMasterConfig) != HAL_OK)
  {
	  return INIT_ERROR;
  }
  return INIT_OK;
}

/**
  * @brief TIM4 Initialization Function
  * This function configures timer that checks led state
  * @param None
  * @retval None
  */
StatusFunctionInitialize_t initializeTimDebounce(void)
{
  TIM_ClockConfigTypeDef sClockSourceConfig = {0};
  TIM_MasterConfigTypeDef sMasterConfig = {0};

  htim4.Instance = TIM4;
  htim4.Init.Prescaler = DebouncePrescaller - 1;
  htim4.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim4.Init.Period = DebouncePeriod - 1;
  htim4.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  htim4.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
  if (HAL_TIM_Base_Init(&htim4) != HAL_OK)
  {
	  return INIT_ERROR;
  }
  sClockSourceConfig.ClockSource = TIM_CLOCKSOURCE_INTERNAL;
  if (HAL_TIM_ConfigClockSource(&htim4, &sClockSourceConfig) != HAL_OK)
  {
	  return INIT_ERROR;
  }
  sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  if (HAL_TIMEx_MasterConfigSynchronization(&htim4, &sMasterConfig) != HAL_OK)
  {
	  return INIT_ERROR;
  }

  return INIT_OK;
}

/**
  * @brief  Get Handle of blinking timer
  * @retval Pointer to Handle of blinking timer
  */
TIM_HandleTypeDef* getTimBlinkHandle(void)
{
	return &htim2;
}

/**
  * @brief  Get Handle of state checking timer
  * @retval Pointer to Handle of state checking timer
  */
TIM_HandleTypeDef* getTimStateCheckHandle(void)
{
	return &htim4;
}

/**
  * @brief  Update period of timer
  * @param  Period: new timer period
  * @param  htim: timer handle
  * @retval Status timer period update
  */
StatusValueUpdate_t updateTimerPeriod(uint32_t Period)
{
    if (Period == PERIOD_200_MS || Period == PERIOD_100_MS)
    {
    	htim2.Init.Period = Period;
        if (HAL_TIM_Base_Init(&htim2) != HAL_OK) {
            return UPDATE_ERROR;
        }
        else {
            return UPDATE_OK;
        }
    }
    else
    {
        return INVALID_PARAMS;
    }
}
