/**
  ******************************************************************************
  * @file           : prj_drv_led.h
  * @brief          : Header for prj_drv_led.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  *
  * Author: dmitriy
  * Created on: Oct 12, 2022
  *
  ******************************************************************************
  */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef PRJ_DRV_LED_H_
#define PRJ_DRV_LED_H_

/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "prj_drv_pwm.h"

/* Exported types ------------------------------------------------------------*/

/**
  * @brief  Timer blinking period
  */
typedef enum {
    PERIOD_200_MS = 199U,
    PERIOD_100_MS = 99U
} Tim2Period;


/* Exported functions prototypes ---------------------------------------------*/
StatusValueUpdate_t initializePwmLed(PwmLed_t*, TIM_HandleTypeDef*, uint32_t, uint16_t);
StatusValueUpdate_t togglePwmLed(PwmLed_t*);
StatusValueUpdate_t increasePwmLedPulse(PwmLed_t*);


#endif /* PRJ_DRV_LED_H_ */
